//package com.binar.challenge4.repository;
//
//import com.binar.challenge4.model.Schedules;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface SchedulesRepository extends JpaRepository<Schedules, Integer> {
//
//    @Query(value = "select * from Schedules s where s.film_id=?1",nativeQuery = true)
//    List<Schedules> findFilmsFromSchedules(Integer filmId);
//}
