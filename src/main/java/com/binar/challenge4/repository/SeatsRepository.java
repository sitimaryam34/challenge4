package com.binar.challenge4.repository;

import com.binar.challenge4.model.Seats;
import com.binar.challenge4.model.Seats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface SeatsRepository extends JpaRepository <Seats, Integer> {
    Seats findSeatsBySeatsCode(String seatsCode);
}
