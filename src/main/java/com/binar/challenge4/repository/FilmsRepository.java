package com.binar.challenge4.repository;

import com.binar.challenge4.model.Films;
import com.binar.challenge4.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilmsRepository extends JpaRepository<Films, Integer> {

    // Update Film by filmCode
    public Films findByFilmCode(String filmCode);

    // Show Film tayang by isShow
    @Query(value = "select * from Films f where f.is_show=true", nativeQuery = true)
    List<Films> showFilm();
}
