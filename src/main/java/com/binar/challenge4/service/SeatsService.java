package com.binar.challenge4.service;

import com.binar.challenge4.model.Seats;
import org.springframework.stereotype.Service;

@Service
public interface SeatsService {

    Seats addSeat(Seats seats);

    Seats getSeat(String seatsCode);
}
