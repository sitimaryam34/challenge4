package com.binar.challenge4.service;

import com.binar.challenge4.model.Films;
import com.binar.challenge4.repository.FilmsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmsServiceImpl implements FilmsService {

    @Autowired
    private FilmsRepository filmsRepository;

    @Override
    public void addFilm(String filmCode, String filmName, Boolean isShow) {
        Films film = new Films();
        film.setFilmCode(filmCode);
        film.setFilmName(filmName);
        film.setIsShow(isShow);
        filmsRepository.save(film);
    }

    @Override
    public void updateFilm(String filmCode, String newFilmCode, String newFilmName, Boolean newIsShow) {
        Films film = filmsRepository.findByFilmCode(filmCode);
        film.setFilmCode(newFilmCode);
        film.setFilmName(newFilmName);
        film.setIsShow(newIsShow);
        filmsRepository.save(film);
    }

    @Override
    public void deleteFilm(Integer filmId) {
        filmsRepository.deleteById(filmId);
    }

    @Override
    public List<Films> showFilm() {
        return filmsRepository.showFilm();
    }
}