package com.binar.challenge4.service;

import com.binar.challenge4.model.Users;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface UsersService {

    void addUser(String username, String email, String password);

    void updateUser(String username, String newUsername, String newEmail, String newPassword);

    void deleteUser(Integer userId);
}