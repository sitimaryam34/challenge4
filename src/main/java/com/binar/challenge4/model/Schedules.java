package com.binar.challenge4.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
public class Schedules {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schedules_id")
    private Integer schedulesId;

    //schedules butuh column films, films gabutuh schedules
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "filmId", nullable = false)
    private Films filmId;

    @Column(name = "tanggal_tayang")
    @Temporal(TemporalType.DATE)
    private Date tanggalTayang;

    @Column(name = "jam_mulai")
    @Temporal(TemporalType.TIME)
    private Date jamMulai;

    @Column(name = "jam_selesai")
    @Temporal(TemporalType.TIME)
    private Date jamSelesai;

    @Column(name = "harga_tiket")
    private String hargaTiket;
}
