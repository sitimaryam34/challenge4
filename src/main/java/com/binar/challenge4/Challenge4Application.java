package com.binar.challenge4;

import com.binar.challenge4.service.FilmsService;
import com.binar.challenge4.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Challenge4Application implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Challenge4Application.class, args);
	}

	@Autowired
	private FilmsService filmsService;

	@Override
	public void run(String... args) throws Exception {
//        filmsService.addSchedule(6, "2022-03-18", "11:45:00", "12:00:00", "25000");
	}
}
