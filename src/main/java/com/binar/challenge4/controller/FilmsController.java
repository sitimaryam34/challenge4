package com.binar.challenge4.controller;

import com.binar.challenge4.model.Films;
import com.binar.challenge4.model.Schedules;
import com.binar.challenge4.model.Users;
import com.binar.challenge4.service.FilmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.List;
/** HTTP Status every method
 * GET : FOUND
 * POST : CREATED
 * PUT : ACCEPTED
 * DELETE : ACCEPTED */

@Tag(name="Films", description = "CRUD API for film and schedule using the Films and Schedules entity in the challenge4 database.")
@RestController
@RequestMapping("/films")

@Controller
public class FilmsController {

    @Autowired
    private FilmsService filmsService;

    // tambah Film
    @Operation(summary = "This API method function to read existing film information by inputting the film name.")
    @GetMapping("/get-film/{filmName}")
    public ResponseEntity getFilmByFilmName(@PathVariable String filmName) {
        Films films = filmsService.getFilmByFilmName(filmName);
        Map<String, Object> respBody = new HashMap<>();
        respBody.put("ID Film", films.getFilmId());
        respBody.put("Code Film", films.getFilmCode());
        respBody.put("Nama Film", films.getFilmName());
        return new ResponseEntity(respBody, HttpStatus.FOUND);

    /*public String addFilm(String filmCode, String filmName, Boolean isShow) {
        filmsService.addFilm(filmCode, filmName, isShow);
        return "Add Film Success!";*/

    }

    // Update Film
    @ApiResponses(value = {
            @ApiResponse(responseCode = "234", description = "Successfully added a new film!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "{" + "\"filmId\":\"1\","+
                                    "\"filmCode\":\"AVG\","+
                                    "\"filmName\":\"Avengers\"," +
                                    "\"isShow\":\"true\"}"))
                    }),
            @ApiResponse(responseCode = "404", description = "Failed to add new film.",
                    content = { @Content})})
    @Operation(summary = "This API method function to add a film by inputting a film code, film name, and on show and will be stored in the Films entity.")
    @PostMapping("/add-film")
    public ResponseEntity addFilm(@Schema(example = "{" + "\"filmId\":\"1\","+
            "\"filmCode\":\"AVG\","+
            "\"filmName\":\"Avengers\"," +
            "\"isShow\":\"true\"}") @RequestBody Films films) {
        filmsService.addFilm(films);
        return new ResponseEntity(films, HttpStatus.CREATED);
    /*public String updateFilm(String filmCode, String newFilmCode, String newFilmName, Boolean newIsShow) {
        filmsService.updateFilm(filmCode, newFilmCode, newFilmName, newIsShow);
        return "Update Film Success!";*/
    }

    // hapus film
    @Operation(summary = "This API method function to delete the film you want by inputting the film id.")
    @DeleteMapping("/delete-film/{filmId}")
    public ResponseEntity deleteFilm(@PathVariable Integer filmId) {
        filmsService.deleteFilm(filmId);
        return new ResponseEntity(filmId, HttpStatus.ACCEPTED);
    /*public String deleteFilm(Integer filmId) {
        filmsService.deleteFilm(filmId);
        return  "Delete Film Success!";*/
    }

    // menampilkan Film yang sedang tayang
    @Operation(summary = "This API method function to read existing film on show information by inputting value true or false.")
    @GetMapping("/get-film-tayang")
    public ResponseEntity getFilmTayang() {
        List<Films> listFilms = filmsService.getFilmTayang();
        return new ResponseEntity(listFilms, HttpStatus.FOUND);
    /*public void showFilm() {
        List<Films> listFilms = filmsService.showFilm();
        listFilms.forEach(films -> System.out.println(films.toString()) );*/
    }
}
/** Unit-test
 * Tambah Film
 public String addFilm(String filmCode, String filmName, Boolean isShow) {
 filmsService.addFilm(filmCode, filmName, isShow);
 return "Add Film Success!";
 }

 * Update Film
 public String updateFilm(String filmCode, String newFilmCode, String newFilmName, Boolean newIsShow) {
 filmsService.updateFilm(filmCode, newFilmCode, newFilmName, newIsShow);
 return "Update Film Success!";
 }

 * Hapus film
 public String deleteFilm(Integer filmId) {
 filmsService.deleteFilm(filmId);
 return "Delete Film Success!";
 }

 * menampilkan Film yang sedang tayang
 public String deleteFilm(@PathVariable Integer filmId) {
 return filmsService.deleteFilm(filmId);
 }

 * Tambah Schedule
 public String addSchedule(Integer filmId, String tanggalTayang, String jamMulai, String jamSelesai, String hargaTiket) {
 filmsService.addSchedule(filmId, tanggalTayang, jamMulai, jamSelesai, hargaTiket);
 return "Add Schedule Success!";
 } */
