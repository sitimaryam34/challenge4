package com.binar.challenge4.controller;

import com.binar.challenge4.model.*;
import com.binar.challenge4.service.FilmsService;
import com.binar.challenge4.service.SchedulesService;
import com.binar.challenge4.service.SeatsService;
import com.binar.challenge4.service.UsersService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Tag(name="Invoice", description = "API to generate ticket reservation file containing user name, schedule, seat, and studio in PDF format.")
@RestController
@RequestMapping("/invoice")
@Controller
public class InvoiceController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private FilmsService filmsService;

    @Autowired
    private SchedulesService schedulesService;

    @Autowired
    private SeatsService seatsService;

    @ApiResponses(value = {
            @ApiResponse(responseCode = "234", description = "Successfully read invoice!",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(example = "http://localhost:8080/invoice/get-tiket"))
                    }),
            @ApiResponse(responseCode = "404", description = "Failed to read invoice.",
                    content = { @Content})})
